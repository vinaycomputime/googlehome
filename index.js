'use strict';

const {smarthome} = require('actions-on-google');
const AWS = require('aws-sdk');
var cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();
var iot = new AWS.Iot();
//PTAC company
var iotdata = new AWS.IotData({ endpoint: 'asyh9zqgbddbc-ats.iot.us-west-2.amazonaws.com' });
var documentClient = new AWS.DynamoDB.DocumentClient();
var reqId;
var tempSetpt = 20.0;
var sysMode = 'heat';
var temperature = 25.0;
var devIdResp;
var device_list;	// = ['SAUPTZ1GW-001E5E02C514'];
var userId;
var thing;
var thingStr;
var device_array = {}
var friendlyName = ['living room', 'Bedroom', 'Kitchen', 'Bathroom', 'Diningroom'];
//HABITAT-159
var errorResponse = 0;
//HABITAT-158
var temperatureF;
	
//const app = smarthome();
const app = smarthome({
  jwt: require('./ptac-b3833.json')
});


app.onSync((body, headers) => {
	console.log(body);
	reqId = body.requestId;
	console.log(body.requestId);	//vinay, OK to get requestId
	    var params = {
			"AccessToken": headers.authorization.substr(7)
		};
		cognitoidentityserviceprovider.getUser(params, function(err, data) {
		    if (err) {
		        console.log(err, err.stack); 
		    }
            else {
                var user = data.Username;
                console.log(user);
				////Fetch info from DynamoDB, Enable later
				var params = {
				  TableName: "UserToDeviceList",
				  IndexName: "UserName-index",
				  KeyConditionExpression: 'UserName = :username',
				  ExpressionAttributeValues: {
					':username' : user
				  }
				}
				documentClient.query(params, function(err, data) {
				if(err) { console.log(err.message); }
				else { console.log(data);
					device_list = JSON.parse(data.Items[0].Own);	//User Id: data.Items[0].userid
					//console.log(device_list.list[0]);
					thing = device_list.list[0];
					device_array = device_list.list;
					console.log(device_array.length);
					//device_array.length = 2;
					//device_array = ["SAUPTZ1GW-001E5E02C514"]
					console.log(thing);
					userId = data.Items[0].userid;
					console.log(userId);
					
					//Get Device name didn't work
	
					}
				});
            }
		});				

  if(device_array.length === 2)
  {
	  return {
    requestId: body.requestId,
    payload: { agentUserId: userId,  //'user-123',
      devices: [
	  { id: device_array[0],
        type: 'action.devices.types.THERMOSTAT',
        traits: ['action.devices.traits.TemperatureSetting'],
        name: { defaultNames: [friendlyName[0]],  name: friendlyName[0], nicknames: [friendlyName[0]]},
        willReportState: false,
        attributes: { availableThermostatModes: ['off','heat','cool'], thermostatTemperatureUnit: 'F' },
        deviceInfo: { manufacturer: 'PTAC', model: 'SAUPTZ1PT868',  hwVersion: '3.2',  swVersion: '11.4' }
      },
	  { id: device_array[1],
        type: 'action.devices.types.THERMOSTAT',
        traits: ['action.devices.traits.TemperatureSetting'],
        name: { defaultNames: [friendlyName[1]],  name: friendlyName[1], nicknames: [friendlyName[1]]},
        willReportState: false,	//true,
        attributes: { availableThermostatModes: ['off','heat','cool'],
          thermostatTemperatureUnit: 'F' },
        deviceInfo: { manufacturer: 'PTAC', model: 'SAUPTZ1PT868',  hwVersion: '3.2',  swVersion: '11.4' }
      }
  	  ]
    }
  };
  }
  else if(device_array.length === 3)
  {
	  	return {
    requestId: body.requestId,
    payload: { agentUserId: userId,
      devices: [
	  { id: device_array[0],
        type: 'action.devices.types.THERMOSTAT',
        traits: ['action.devices.traits.TemperatureSetting'],
        name: { defaultNames: [friendlyName[0]],  name: friendlyName[0], nicknames: [friendlyName[0]]},
        willReportState: false,	//true,
        attributes: { availableThermostatModes: ['off','heat','cool'], thermostatTemperatureUnit: 'F' },
        deviceInfo: { manufacturer: 'PTAC', model: 'SAUPTZ1PT868',  hwVersion: '3.2',  swVersion: '11.4' }
      },
	  { id: device_array[1],
        type: 'action.devices.types.THERMOSTAT',
        traits: ['action.devices.traits.TemperatureSetting'],
        name: { defaultNames: [friendlyName[1]],  name: friendlyName[1], nicknames: [friendlyName[1]]},
        willReportState: false,	//true,
        attributes: { availableThermostatModes: ['off','heat','cool'],
          thermostatTemperatureUnit: 'F' },
        deviceInfo: { manufacturer: 'PTAC', model: 'SAUPTZ1PT868',  hwVersion: '3.2',  swVersion: '11.4' }
      },
	  { id: device_array[2],
        type: 'action.devices.types.THERMOSTAT',
        traits: ['action.devices.traits.TemperatureSetting'],
        name: { defaultNames: [friendlyName[2]],  name: friendlyName[2], nicknames: [friendlyName[2]]},
        willReportState: false,	//true,
        attributes: { availableThermostatModes: ['off','heat','cool'],
          thermostatTemperatureUnit: 'F' },
        deviceInfo: { manufacturer: 'PTAC', model: 'SAUPTZ1PT868',  hwVersion: '3.2',  swVersion: '11.4' }
      }
  	  ]
    }
  };
  }
  else if(device_array.length === 4)
  {
	 	return {
    requestId: body.requestId,
    payload: { agentUserId: userId,
      devices: [
	  { id: device_array[0],
        type: 'action.devices.types.THERMOSTAT',
        traits: ['action.devices.traits.TemperatureSetting'],
        name: { defaultNames: [friendlyName[0]],  name: friendlyName[0], nicknames: [friendlyName[0]]},
        willReportState: false,	//true,
        attributes: { availableThermostatModes: ['off','heat','cool'], thermostatTemperatureUnit: 'F' },
        deviceInfo: { manufacturer: 'PTAC', model: 'SAUPTZ1PT868',  hwVersion: '3.2',  swVersion: '11.4' }
      },
	  { id: device_array[1],
        type: 'action.devices.types.THERMOSTAT',
        traits: ['action.devices.traits.TemperatureSetting'],
        name: { defaultNames: [friendlyName[1]],  name: friendlyName[1], nicknames: [friendlyName[1]]},
        willReportState: false,	//true,
        attributes: { availableThermostatModes: ['off','heat','cool'],
          thermostatTemperatureUnit: 'F' },
        deviceInfo: { manufacturer: 'PTAC', model: 'SAUPTZ1PT868',  hwVersion: '3.2',  swVersion: '11.4' }
      },
	  { id: device_array[2],
        type: 'action.devices.types.THERMOSTAT',
        traits: ['action.devices.traits.TemperatureSetting'],
        name: { defaultNames: [friendlyName[2]],  name: friendlyName[2], nicknames: [friendlyName[2]]},
        willReportState: false,	//true,
        attributes: { availableThermostatModes: ['off','heat','cool'],
          thermostatTemperatureUnit: 'F' },
        deviceInfo: { manufacturer: 'PTAC', model: 'SAUPTZ1PT868',  hwVersion: '3.2',  swVersion: '11.4' }
      },
	  { id: device_array[3],
        type: 'action.devices.types.THERMOSTAT',
        traits: ['action.devices.traits.TemperatureSetting'],
        name: { defaultNames: [friendlyName[3]],  name: friendlyName[3], nicknames: [friendlyName[3]]},
        willReportState: false,	//true,
        attributes: { availableThermostatModes: ['off','heat','cool'],
          thermostatTemperatureUnit: 'F' },
        deviceInfo: { manufacturer: 'PTAC', model: 'SAUPTZ1PT868',  hwVersion: '3.2',  swVersion: '11.4' }
      }
  	  ]
    }
  }; 
  }
  else if(device_array.length === 5)
  {
		return {
    requestId: body.requestId,
    payload: { agentUserId: userId,
      devices: [
	  { id: device_array[0],
        type: 'action.devices.types.THERMOSTAT',
        traits: ['action.devices.traits.TemperatureSetting'],
        name: { defaultNames: [friendlyName[0]],  name: friendlyName[0], nicknames: [friendlyName[0]]},
        willReportState: false,	//true,
        attributes: { availableThermostatModes: ['off','heat','cool'], thermostatTemperatureUnit: 'F' },
        deviceInfo: { manufacturer: 'PTAC', model: 'SAUPTZ1PT868',  hwVersion: '3.2',  swVersion: '11.4' }
      },
	  { id: device_array[1],
        type: 'action.devices.types.THERMOSTAT',
        traits: ['action.devices.traits.TemperatureSetting'],
        name: { defaultNames: [friendlyName[1]],  name: friendlyName[1], nicknames: [friendlyName[1]]},
        willReportState: false,	//true,
        attributes: { availableThermostatModes: ['off','heat','cool'],
          thermostatTemperatureUnit: 'F' },
        deviceInfo: { manufacturer: 'PTAC', model: 'SAUPTZ1PT868',  hwVersion: '3.2',  swVersion: '11.4' }
      },
	  { id: device_array[2],
        type: 'action.devices.types.THERMOSTAT',
        traits: ['action.devices.traits.TemperatureSetting'],
        name: { defaultNames: [friendlyName[2]],  name: friendlyName[2], nicknames: [friendlyName[2]]},
        willReportState: false,	//true,
        attributes: { availableThermostatModes: ['off','heat','cool'],
          thermostatTemperatureUnit: 'F' },
        deviceInfo: { manufacturer: 'PTAC', model: 'SAUPTZ1PT868',  hwVersion: '3.2',  swVersion: '11.4' }
      },
	  { id: device_array[3],
        type: 'action.devices.types.THERMOSTAT',
        traits: ['action.devices.traits.TemperatureSetting'],
        name: { defaultNames: [friendlyName[3]],  name: friendlyName[3], nicknames: [friendlyName[3]]},
        willReportState: false,	//true,
        attributes: { availableThermostatModes: ['off','heat','cool'],
          thermostatTemperatureUnit: 'F' },
        deviceInfo: { manufacturer: 'PTAC', model: 'SAUPTZ1PT868',  hwVersion: '3.2',  swVersion: '11.4' }
      },
	  { id: device_array[4],
        type: 'action.devices.types.THERMOSTAT',
        traits: ['action.devices.traits.TemperatureSetting'],
        name: { defaultNames: [friendlyName[4]],  name: friendlyName[4], nicknames: [friendlyName[4]]},
        willReportState: false,	//true,
        attributes: { availableThermostatModes: ['off','heat','cool'],
          thermostatTemperatureUnit: 'F' },
        deviceInfo: { manufacturer: 'PTAC', model: 'SAUPTZ1PT868',  hwVersion: '3.2',  swVersion: '11.4' }
      }
  	  ]
    }
  };   
  }
  else
  {
  return {
    requestId: body.requestId,
    payload: {
      agentUserId: userId, 
      devices: [{
		id: device_array[0],
        type: 'action.devices.types.THERMOSTAT',
        traits: [
          'action.devices.traits.TemperatureSetting'
        ],
        name: {
          defaultNames: [friendlyName[0]],
          name: friendlyName[0],
          nicknames: [friendlyName[0]]
        },
        willReportState: true,	//false,
        attributes: {
          availableThermostatModes: ['off','heat','cool'],	//20201027: new form should use array
          thermostatTemperatureUnit: 'F'//,
		  //queryOnlyTemperatureSetting: true	//Do Not Enable this otherwise onExecute is never called
        },
        deviceInfo: {
          manufacturer: 'PTAC',
          model: 'SAUPTZ1PT868',	//'SAUPTZ1GW',
          hwVersion: '3.2',
          swVersion: '11.4'
        }/*,	//20210115
        customData: {
          fooValue: 74,
          barValue: true,
          bazValue: 'lambtwirl'
        }*/
      }
  	  ]
    }
  };
  }
});


app.onQuery((body, headers) => {
	console.log('Query request');
	reqId = body.requestId;
	//console.log(headers.authorization.substr(7));	//received token in every request
	var devId = body.inputs[0].payload.devices[0].id
	var devIdStr = devId.toString();
	devIdResp = "\'" + devIdStr + "\'";
	var shadow;
	var heatingsetpointF;
	var heatingsetpoint;
	var heatingsetpointx100;
	var coolingsetpoint;
	var systemmode;
	//var temperature = 25.0;
	var runningmode;
	
	const payload = {
    devices: {},
	};
	const intent = body.inputs[0];
	const device = intent.payload.devices;
	var thingGrp = 'Gateway-' + devId.split("-")[1];
	console.log(thingGrp);
	var paramsThing = {
		//"thingGroupName": 'Gateway-001E5E02C514',
		"thingGroupName": thingGrp,
		"maxResults": 2			//change to 5 to handle maximum device model
    };
	iot.listThingsInThingGroup(paramsThing, function(err, data){
	if (err) {
	        console.log(err, err.stack); 
	    }
        else {
			console.log(data);
			//Extract PT868 index:
			var thngSearch = devId + '-SAUPTZ1PT868-0000000000000000';
			var idx = data.things.indexOf(thngSearch)
			console.log('PT868 index : ' + idx);

			console.log(data.things[idx]); //PT868 model
			var deviceId = data.things[idx];
	
	var params = {
		"thingName": deviceId
    };

    iotdata.getThingShadow(params, function(err, data){
	if (err) {
	        console.log(err, err.stack); 
	    }
        else {
            console.log(data);
			shadow = JSON.parse(data.payload);
			console.log(shadow);
			//getModelDescriptionbyModelID - get model description from database
					//var model = thing.split("-")[0]
				//var modelParams = {
				//TableName: 'ModelTable',
				//KeyConditionExpression: 'ModelID = :ModelID',
				//ExpressionAttributeValues: {
				//	':ModelID' : 'ST921WF' //model
				//}
				//}
				//documentClient.query(modelParams, function(err, data){
				//if (err) {
				//		console.log(err, err.stack); 
				//	}
				//	else {
						//console.log(data.Items[0]);
						
						var reportstate = shadow.state.reported;
						var desiredstate = shadow.state.desired;
						//var reportproperties = reportstate[data.Items[0].nodeID].properties;
						var reportproperties = reportstate['000000000003'].properties;
						console.log(reportproperties);				
						//var propertiesprefix = data.Items[0].NodeEndpoint;
						temperature = reportproperties["ep0:sPTAC868:LocalTemperature_x100"];
						//HABITAT-158 debug, convert to F and math.round(F) and again back to C
						//temperature = temperature-10
						temperature = temperature/100;						
						console.log(temperature);
						temperatureF = convertTemperature(temperature , "CELSIUS", "FAHRENHEIT")
						temperatureF = Math.round(temperatureF)
						temperature = convertTemperature(temperatureF , "FAHRENHEIT", "CELSIUS")
						
						heatingsetpointx100 = reportproperties["ep0:sPTAC868:HeatingSetpoint_x100"];
						heatingsetpoint = heatingsetpointx100/100;
						console.log(heatingsetpoint);
						tempSetpt = heatingsetpoint;
						heatingsetpointF = convertTemperature(heatingsetpoint , "CELSIUS", "FAHRENHEIT")
						//console.log(heatingsetpointF);
						coolingsetpoint = reportproperties["ep0:sPTAC868:CoolingSetpoint_x100"];
						coolingsetpoint = coolingsetpoint/100
						console.log(coolingsetpoint);
						systemmode = reportproperties["ep0:sPTAC868:SystemMode"];
						console.log(systemmode);
					    runningmode = reportproperties["ep0:sPTAC868:RunningMode"];
						console.log(runningmode);
						//friendlyName = reportproperties[propertiesprefix+ ":sGateway:DeviceName"];
						//console.log(friendlyName);
						//if(friendlyName !== 'living room'){
						//app.requestSync('us-west-2:cdc03960-5bfb-4e32-92b3-0807a5cb8f6a').then((res) => {console.log('Request sync was successful');})
						//.catch((res) => {console.log('Request sync failed');});}

						switch(systemmode) {
							case 0:
								sysMode = 'off'
								break
							case 1:
								sysMode = 'heatcool'
								//sysMode = 'auto'
								if (runningmode === 3)
								{
									sysMode = 'cool'
									tempSetpt = coolingsetpoint
								}
								else if (runningmode === 4)
								{
									sysMode = 'heat'
									tempSetpt = heatingsetpoint
								}
								break
							case 3:
								sysMode = 'cool'
								tempSetpt = coolingsetpoint
								break
							case 4:
								sysMode = 'heat'
								tempSetpt = heatingsetpoint
								break    
						}
						  
						//return response here did not work

				//	}
				//});
				
						//return response here did not work

		}
	});
		}
	});

	//iot.describeThing(params, function(err, data){
	//if (err) {
	//        console.log(err, err.stack); 
	//    }
    //    else {
    //        console.log(data);
	//		//getDeviceConfigFromIotThingShadow(data);
	//	}
	//});
   

//console.log(devId);
console.log(devIdResp);
//const queryRespStr = '{ requestId: '+body.requestId+', payload: { devices: { \'ST921WF-001E5E02F6F8\': { online: true, thermostatMode: \'cool\', thermostatTemperatureSetpoint: '+tempSetpt+', thermostatTemperatureAmbient: '+temperature+', status: \'SUCCESS\' } } } }';

  console.log(device[0].id);
	   payload.devices[device[0].id] = {
         online: true,
         thermostatMode: sysMode,
         thermostatTemperatureSetpoint: tempSetpt,
		 thermostatTemperatureAmbient: temperature,
         status: 'SUCCESS'
       };
  return {
    requestId: body.requestId,
    payload: payload,
  };

});


app.onExecute((body, headers) => {
	var cmdExe = body.inputs[0].payload.commands[0];
	reqId = body.requestId;
	console.log('Execute request'+ cmdExe);
	//console.log(body.inputs[0].payload.commands[0].devices[0].id)
	var devId = body.inputs[0].payload.commands[0].devices[0].id;
	var devIdResp = "\'"+devId+"\'";
	//console.log('Execute request '+ cmdExe.execution[0].params.thermostatTemperatureSetpoint);
	//console.log(headers.authorization.substr(7));	//received token in every request
	var nodeid = '000000000003';	//modeldescription.nodeID
	var thingName = devId	
	var desireproperties = {}
	var shadow;
	var systemmode;
	var temperature;
	let desiredshadowState = {
		state: {
			//desired: {
			//}
		}
	};

	var thingGrp = 'Gateway-' + devId.split("-")[1];
	console.log(thingGrp);	var paramsThing = {
		//"thingGroupName": 'Gateway-001E5E02C514',
		"thingGroupName": thingGrp,
		"maxResults": 2
    };
	iot.listThingsInThingGroup(paramsThing, function(err, data){
	if (err) {
	        console.log(err, err.stack); 
	    }
        else {
			console.log(data);
			//Extract PT868 index:
			var thngSearch = devId + '-SAUPTZ1PT868-0000000000000000';
			var idx = data.things.indexOf(thngSearch)
			console.log('PT868 index : ' + idx);

			console.log(data.things[idx]);
			var deviceId = data.things[idx];
	
	if(cmdExe.execution[0].params.thermostatTemperatureSetpoint){
		console.log('Received thermostatTemperatureSetpoint....');	
		tempSetpt = cmdExe.execution[0].params.thermostatTemperatureSetpoint;
	
		var params = {
		"thingName":	deviceId
		};
    iotdata.getThingShadow(params, function(err, data){
	if (err) {
	        console.log(err, err.stack); 
	    }
        else {
			shadow = JSON.parse(data.payload);
			console.log(shadow);
				//var modelParams = {
				//TableName: 'DeviceOTATable',
				//KeyConditionExpression: 'ModelID = :ModelID',
				//ExpressionAttributeValues: {
				//	':ModelID' : 'SAUPTZ1GW'	//devId.split("-")[0]
				//}
				//}
				//documentClient.query(modelParams, function(err, data){
				//if (err) {
				//		console.log(err, err.stack); 
				//	}
				//	else {
				//		console.log(data.Items[0]);
						var reportstate = shadow.state.reported;
						var reportproperties = reportstate['000000000003'].properties;
						systemmode = reportproperties["ep0:sPTAC868:SystemMode"];
						if(systemmode == 0)
						errorResponse = 1;
					
						//var propertiesprefix = data.Items[0].NodeEndpoint;
						temperature = reportproperties["ep0:sPTAC868:LocalTemperature_x100"];
						temperature = temperature/100;
					    var runingmod = reportproperties["ep0:sPTAC868:RunningMode"];
						//read running mode
						/*if(systemmode == 3)
						{
							desireproperties["ep0:sPTAC868:SetCoolingSetpoint_x100"] = tempSetpt*100
							desiredshadowState.state[nodeid] = {properties: desireproperties}
							updateThingShadow(deviceId, desiredshadowState);							
						}
						else if(systemmode == 4)
						{
							desireproperties["ep0:sPTAC868:SetHeatingSetpoint_x100"] = tempSetpt*100
							desiredshadowState.state[nodeid] = {properties: desireproperties}
							updateThingShadow(deviceId, desiredshadowState);							
						}
						else if(systemmode == 0)
						{	//HABITAT-159
							console.log('Thermostat is OFF....');
							return {
								requestId: body.requestId,
								payload: {
								commands: [{
									ids: [device[0].id],		
									status: 'ERROR',
									errorCode: "inOffMode"
								}]}
							};
						}							
						else if(systemmode == 1)
						{	
							//HABITAT-159
							if(runingmod == 3)
							{
							desireproperties["ep0:sPTAC868:SetCoolingSetpoint_x100"] = tempSetpt*100
							desiredshadowState.state[nodeid] = {properties: desireproperties}
							updateThingShadow(deviceId, desiredshadowState);								
							}	
							else if(runingmod == 4)
							{
							desireproperties["ep0:sPTAC868:SetHeatingSetpoint_x100"] = tempSetpt*100
							desiredshadowState.state[nodeid] = {properties: desireproperties}
							updateThingShadow(deviceId, desiredshadowState);								
							}								
						}*/							
						switch(systemmode) {
							case 3:
							errorResponse = 0;
							desireproperties["ep0:sPTAC868:SetCoolingSetpoint_x100"] = tempSetpt*100
							desiredshadowState.state[nodeid] = {properties: desireproperties}
							updateThingShadow(deviceId, desiredshadowState);
							console.log('SetCoolingSetpoint_x100');
							break
							case 4:
							errorResponse = 0;
							desireproperties["ep0:sPTAC868:SetHeatingSetpoint_x100"] = tempSetpt*100
							desiredshadowState.state[nodeid] = {properties: desireproperties}
							updateThingShadow(deviceId, desiredshadowState);
							break
							//HABITAT-159
							case 0:
							errorResponse = 1;
							break							
							//handle case 1 for auto based on running mode	
							case 1:
							errorResponse = 0;
							if(runingmod === 3){
							desireproperties["ep0:sPTAC868:SetCoolingSetpoint_x100"] = tempSetpt*100
							desiredshadowState.state[nodeid] = {properties: desireproperties}
							updateThingShadow(deviceId, desiredshadowState);
							}
							else
							if(runingmod === 4){
							desireproperties["ep0:sPTAC868:SetHeatingSetpoint_x100"] = tempSetpt*100
							desiredshadowState.state[nodeid] = {properties: desireproperties}
							updateThingShadow(deviceId, desiredshadowState);								
							}								
							break
						}
						
				//	}
				//});
			}
		});
	}
	else if(cmdExe.execution[0].params.thermostatMode){
	sysMode = cmdExe.execution[0].params.thermostatMode;
		errorResponse = 0;
		//data.Items[0].NodeEndpoint -> ep0
		if(sysMode == 'cool'){
		desireproperties["ep0:sPTAC868:SetSystemMode"] = 3
		desireproperties["ep0:sPTAC868:SetFanMode"] = 5		//HABITAT-156
		desiredshadowState.state[nodeid] = {properties: desireproperties}
		updateThingShadow(deviceId, desiredshadowState);
		}
		else if(sysMode == 'heat'){
		desireproperties["ep0:sPTAC868:SetSystemMode"] = 4
		desireproperties["ep0:sPTAC868:SetFanMode"] = 5		//HABITAT-156		
		desiredshadowState.state[nodeid] = {properties: desireproperties}
		updateThingShadow(deviceId, desiredshadowState);		
		}
		else if(sysMode == 'off'){
		desireproperties["ep0:sPTAC868:SetSystemMode"] = 0
		desiredshadowState.state[nodeid] = {properties: desireproperties}
		updateThingShadow(deviceId, desiredshadowState);		
		}	
		//handle heatcool
		else if(sysMode == 'heatcool'){
		desireproperties["ep0:sPTAC868:SetSystemMode"] = 1
		desiredshadowState.state[nodeid] = {properties: desireproperties}
		updateThingShadow(deviceId, desiredshadowState);		
		}	
		else if(sysMode == 'auto'){
		desireproperties["ep0:sPTAC868:SetSystemMode"] = 1
		desireproperties["ep0:sPTAC868:SetFanMode"] = 5		//HABITAT-156		
		desiredshadowState.state[nodeid] = {properties: desireproperties}
		updateThingShadow(deviceId, desiredshadowState);		
		}	
	}
		}
		});
	//desireproperties["ep0:sWIFITherS:SetHeatingSetpoint_x100"] = tempSetpt*100
	
	//desiredshadowState.state.desired[nodeid] = {properties: desireproperties}
	//updateThingShadow(thingName, desiredshadowState);

  const intent = body.inputs[0];
  const cmd = intent.payload.commands[0];
  const device = cmd.devices;
  console.log(device[0].id);
  //HABITAT-159
  //setTimeout(function(){
  if(errorResponse == 1)
  {		
	return {
		requestId: body.requestId,
		payload: {
		commands: [{
			ids: [device[0].id],		
			status: 'ERROR',
			errorCode: "inOffMode"
		}]}
	};
  }  
  else
  {		
	return {
		requestId: body.requestId,
		payload: {
		commands: [{
			ids: [device[0].id],		
			status: 'SUCCESS',
			states: {
			thermostatMode: sysMode,
			thermostatTemperatureSetpoint: tempSetpt,
			thermostatTemperatureAmbient: temperature
			}
		}]}
	};
  }//}, 0);
 
});

// New device is added for user
/*app.requestSync('us-west-2:cdc03960-5bfb-4e32-92b3-0807a5cb8f6a')
  .then((res) => {
    console.log('Request sync was successful');
  })
  .catch((res) => {
    console.log('Request sync failed');
  });
*/

function updateThingShadow(thingName, shadowState) {
    try {
        var desiredstate = shadowState.state;
        //desiredstate["version"] = 1
        //desiredstate["createdAt"] = new Date().toISOString()
        
        var payloadjson = JSON.stringify(shadowState)
        console.log(payloadjson)  //vinay
        var sendtopic = "$aws/things/" + thingName + "/shadow/update"
//       payloadjson = '{{"state":{"desired":{"000000000003":{"properties":{"ep0:sWIFITherS:SetHeatingSetpoint_x100":1946}},"version":1,"createdAt":"2020-08-31T01:32:58.735Z"}}}}'
        var params = {
            payload: Buffer.from(payloadjson),
            topic: sendtopic,
            qos: '1'
        };
        // Updating shadow updates our *control plane*, but it doesn't necessarily
        // mean our device state has changed. The device is responsible for monitoring
        // the device shadow and responding to changes in desired states. 
        //log(`Calling iotdata.publish() with params:`, params);    //vinay
        iotdata.publish(params, function(err, data){
			if(err) console.log(err, err.stack);
			else console.log(data);
		});
    }
    catch (err) {
        console.log('Error: unable to update device shadow:', err);
        throw (err);
    }
}

function convertTemperature(temperature, currentScale, desiredScale) {
    if (currentScale === desiredScale) {
        return temperature;
    }
    else if (currentScale === 'FAHRENHEIT' && desiredScale === 'CELSIUS') {
        return convertFahrenheitToCelsius(temperature);
    }
    else if (currentScale === 'CELSIUS' && desiredScale === 'FAHRENHEIT') {
        return convertCelsiusToFahrenheit(temperature);
    }
    else {
        throw new Error(`Unable to convert ${currentScale} to ${desiredScale}, unsupported temp scale.`);
    }
} 

function convertCelsiusToFahrenheit(celsius) {
    var fahrenheit = Math.round(((celsius * 1.8) + 32) * 100);
    //log(`Converted temperature to ${fahrenheit} FAHRENHEIT.`);
    return fahrenheit/100;
}

function convertFahrenheitToCelsius(fahrenheit) {
    var celsius = Math.round(((fahrenheit - 32) * 0.556)*100);
    //log(`Converted temperature to ${celsius} CELSIUS.`);  //vinay
    return celsius / 100;
}
 
//exports.smarthome = functions.https.onRequest(app);
exports.smarthome = app;
